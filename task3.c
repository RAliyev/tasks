#include <stdio.h>

int main() {
    int n;
    scanf("%d", &n);

    int scores[n];


    int totalScore = 0;

    for (int i = 0; i < n; i++) {
        scanf("%d", &scores[i]);
    }

    for (int i = 0; i < n; i++) {
        totalScore += scores[i];
    }

    double averageScore = (double)totalScore / n;

    printf("Average Score: %.2lf\n", averageScore);

    return 0;
}
