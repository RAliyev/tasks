#include <stdio.h>

void mergeSortedArrays(int arr1[], int m, int arr2[], int n, int result[]) {
    int i = 0;
    int j = 0;
    int k = 0;

    while (i < m && j < n) {
        if (arr1[i] < arr2[j]) {
            result[k] = arr1[i];
            i++;
        } else {
            result[k] = arr2[j];
            j++;
        }
        k++;
    }

    while (i < m) {
        result[k] = arr1[i];
        i++;
        k++;
    }

    while (j < n) {
        result[k] = arr2[j];
        j++;
        k++;
    }
}

int main() {

    int m;
    scanf("%d", &m);
    int arr1[m];
    for (int i = 0; i < m; i++)
        scanf("%d", &arr1[i]);


    int n;
    scanf("%d", &n);
    int arr2[n];
    for (int i = 0; i < n; i++)
        scanf("%d", &arr2[i]);


    int result[m + n];

    mergeSortedArrays(arr1, m, arr2, n, result);

    printf("Merged and sorted array: ");
    for (int i = 0; i < m + n; i++) {
        printf("%d ", result[i]);
    }
    printf("\n");

    return 0;
}
