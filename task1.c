#include <stdio.h>
#include <string.h>

void findSubstringOccurrences(const char *text, const char *substring) {
    int textLength = strlen(text);
    int subLength = strlen(substring);


    int i, j;
    for (i = 0; i <= textLength - subLength; i++) {
        int found = 1;

        for (j = 0; j < subLength - 1; j++) {
            if (text[i + j] != substring[j]) {
                found = 0;
                break;
            }
        }

        if (found) {
            printf("Found '%s' at position %d", substring, i);
        }
    }
}

int main() {
    char text[100];
    char substring[100];

    fgets(text, sizeof(text), stdin);

    fgets(substring, sizeof(substring), stdin);

    printf("Text: %s\n", text);
    printf("Finding all occurrences of '%s' in the text:\n", substring);

    findSubstringOccurrences(text, substring);

    return 0;
}
