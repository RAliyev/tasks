#include <stdio.h>
#include <stdbool.h>

void removeDuplicates(int arr[], int n) {
    if (n <= 1) {
        return;
    }

    int newSize = 1;

    for (int i = 1; i < n; i++) {
        bool isDuplicate = 0;

        for (int j = 0; j < newSize; j++) {
            if (arr[i] == arr[j]) {
                isDuplicate = 1;
                break;
            }
        }

        if (!isDuplicate) {
            arr[newSize] = arr[i];
            newSize++;
        }
    }

    printf("Array with duplicates removed: ");
    for (int i = 0; i < newSize; i++) {
        printf("%d ", arr[i]);
    }
}

int main() {
    int n;
    scanf("%d", &n);
    int arr[n];

    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }


    removeDuplicates(arr, n);

    return 0;
}
